package com.thoughtWorks.shoppingExperience;

import java.util.logging.Logger;

class CartTest {

    private static final Logger LOGGER = Logger.getLogger(CartTest.class.getName());

    @org.junit.jupiter.api.Test
    void testToString() {
        Cart cart = new Cart();
        cart.addItemToCart(new SellableProduct("Brand1","Type1",
                1,"Test description",1233));
        LOGGER.info(cart.toString());
    }

}