package com.thoughtWorks.shoppingExperience;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class WebsiteOwnerTest {


    @Test
    void displayAllItemsForSale() {
        WebsiteOwnerInterface websiteOwner = new WebsiteOwner();
        SellableProduct product1 = new SellableProduct("adidas","shoe-1",
                22,"brown shoe",1234);
        SellableProduct product2 = new SellableProduct("adidas","shoe-2",
                22,"brown shoe",9989);
        websiteOwner.addItemsToSell(product1);
        websiteOwner.addItemsToSell(product2);
        websiteOwner.displayAllItemsForSale();
    }

    @Test
    void addItemsToSell() {
        WebsiteOwnerInterface websiteOwner = new WebsiteOwner();
        SellableProduct product1 = new SellableProduct("adidas","shoe-1",
                22,"brown shoe",1234);
        websiteOwner.addItemsToSell(product1);
        List<SellableProduct> products = websiteOwner.getAllProducts();
        assertEquals(products.size(),1);
        assertEquals(products.get(0).getSkuNumber(),1234);

        SellableProduct product2 = new SellableProduct("adidas","shoe-2",
                22,"brown shoe",9989);
        websiteOwner.addItemsToSell(product2);
        products = websiteOwner.getAllProducts();
        assertEquals(products.size(),2);

        SellableProduct product3 = new SellableProduct("adidas","shoe",
                22,"brown shoe",1234);
        SellableProduct product4 = new SellableProduct("adidas","shoe",
                22,"brown shoe",9989);

        List<SellableProduct> addedProducts = Arrays.asList(product3, product4);

        assertTrue(addedProducts.containsAll(products));
    }

    @Test
    void displayItemsOfABrand() {
    }


}