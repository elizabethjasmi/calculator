package com.thoughtWorks.shoppingExperience;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    void addToMyCart() {
    UserInterface customer = new User("Test_user1");
    SellableProduct product1 = new SellableProduct("adidas","shoe-1",
                22,"brown shoe",1234);
    Cart cart = customer.addToMyCart(product1);
    assertEquals(cart.getCartitems().get(0) , product1);

    }

    @Test
    void seeMyCart() {
        //browse the website
        WebsiteInterface website = WebsiteOwner.getWebsite();
        List<SellableProduct> products = website.getAllProducts();
        // add to cart an existing product
        UserInterface customer = new User("Test_user1");
        customer.addToMyCart(products.get(0));
        assertEquals(customer.seeMyCart().getCartitems().size(),1);

    }
}