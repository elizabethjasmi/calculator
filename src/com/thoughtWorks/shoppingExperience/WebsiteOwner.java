package com.thoughtWorks.shoppingExperience;

import java.util.TreeMap;
import java.util.*;
import java.util.Collection;
import java.util.Iterator;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.stream.Collectors;


//import Users.vapasi.IdeaProjects.ClassAssignments.src.*;
public class WebsiteOwner implements WebsiteOwnerInterface {
    private static final Logger LOGGER = Logger.getLogger(WebsiteOwner.class.getName());
    private Map<Integer, SellableProduct> itemsToSell = new TreeMap<Integer, SellableProduct>();

    /**
     * this is a dummy website implementation with 2 sample products
     * @return
     */
    public static WebsiteInterface getWebsite(){
        WebsiteOwnerInterface website = new WebsiteOwner();

        SellableProduct product1 = new SellableProduct("adidas","shoe",
                22,"brown shoe",1234);
        website.addItemsToSell(product1);
        SellableProduct product2 = new SellableProduct("adidas","shoe",
                22,"brown shoe",9989);
        website.addItemsToSell(product2);
        return website;
    }
    @Override
    public void displayAllItemsForSale() {

        Collection<SellableProduct> c = itemsToSell.values();
        //obtain an Iterator for Collection
        Iterator<SellableProduct> itr = c.iterator();
        //iterate through TreeMap values iterator
        while (itr.hasNext()) {
            SellableProduct item = itr.next();
            System.out.println(item.getBrandName() + " " + item.getProductType() + " " + item.getProductName() + " " + item.getAvailableStock());
        }
    }

    @Override
    public void addItemsToSell(SellableProduct product) {
        itemsToSell.put(product.getSkuNumber(), product);
    }

    @Override
    public List<SellableProduct> getAllProducts() {
        return itemsToSell.values().stream().collect(Collectors.toList());
    }

    @Override
    public List<SellableProduct> getAllProductsOfBrand(String brandName) {
        return itemsToSell.values().stream().filter(i ->i.getBrandName().equalsIgnoreCase(brandName)).collect(Collectors.toList());
    }


    @Override
    public void displayItemsOfABrand(String brandName) {
        List<SellableProduct> itemsOfABrand;
        itemsOfABrand = itemsToSell.values().stream().filter(item -> item.getBrandName().equalsIgnoreCase(brandName)).collect(Collectors.toList());
        itemsOfABrand.forEach(System.out::println);
    }


    public void main(String args[]) {
        WebsiteOwner siteOwner = new WebsiteOwner();
        readProductsFromUserInput(siteOwner);
        String brandName = readBrandFromUserInput();
        siteOwner.displayItemsOfABrand(brandName);
    }

    private String readBrandFromUserInput() {
        Scanner reader = new Scanner(System.in);
        String brandName;
        System.out.println("Which brand's item do you want to see?");
        brandName = reader.next();
        return brandName;
    }

    private Scanner readProductsFromUserInput(WebsiteOwner siteOwner) {
        Scanner reader = new Scanner(System.in);
        boolean hasMoreToAdd = true;
        String quitResponse = "no";
        while (hasMoreToAdd) {
            System.out.println("Enter BRAND NAME\n");
            String brandName = reader.next();
            System.out.println("Enter PRODUCT TYPE\n");
            String productType = reader.next();
            System.out.println("Enter NUMBER OF ITEMS AVAILABLE\n");
            Integer availableStock = reader.nextInt();
            System.out.println("Enter PRODUCT NAME\n");
            String description = reader.next();
            System.out.println("Enter SKU Number\n");
            Integer skuNumber = reader.nextInt();
            System.out.println("Do you want to add more products -type YES to add\n");
            quitResponse = reader.next();

            SellableProduct product = new SellableProduct(brandName, productType, availableStock, description, skuNumber);
            addItemsToSell(product);

            if (quitResponse.equalsIgnoreCase("YES") || quitResponse.equalsIgnoreCase("Y")) {
                hasMoreToAdd = true;
            }
            else {
                hasMoreToAdd = false;
            }

        }
        siteOwner.displayAllItemsForSale();
        return reader;
    }


}

