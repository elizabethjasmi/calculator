package com.thoughtWorks.shoppingExperience;

import java.util.Objects;

public class SellableProduct {

    private String brandName;
    private String productType;
    private int availableStock;
    private String productName;
    private Integer skuNumber;

    public SellableProduct(String brandName, String productType, int availableStock, String description, Integer skuNumber ) {
        this.brandName = brandName;
        this.productType = productType;
        this.availableStock = availableStock;
        this.productName =description;
        this.skuNumber = skuNumber;
    }

    @Override
    public String toString(){
        String itemDetailString = this.brandName + "" + this.productType + " " + this.availableStock + " " + this.productName+ " "+ this.skuNumber;
        return itemDetailString;
    }

    public Integer getSkuNumber() {
        return skuNumber;
    }

    public void setSkuNumber(Integer skuNumber) {
        this.skuNumber = skuNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SellableProduct)) {
            return false;
        }
        SellableProduct otherProduct = (SellableProduct) obj;
        return Objects.equals(otherProduct.skuNumber, skuNumber);
    }


    public String getBrandName() {
        return brandName;
    }

    public String getProductType() {
        return productType;
    }

    public int getAvailableStock() {
        return availableStock;
    }

    public String getProductName() {
        return productName;
    }
}

