package com.thoughtWorks.shoppingExperience;

/**
 * This is the interface for a registered user
 */
public interface UserInterface {
    public Cart addToMyCart(SellableProduct product);
    public Cart seeMyCart();

}
