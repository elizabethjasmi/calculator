package com.thoughtWorks.shoppingExperience;

import java.util.logging.Logger;

public class User implements UserInterface{
    private static final Logger LOGGER = Logger.getLogger(User.class.getName());

    private String userName;
    private Cart cart;

    public User(String userName) {
        this.userName = userName;
        this.cart = new Cart();
    }

    @Override
    public Cart addToMyCart(SellableProduct product) {
        cart.addItemToCart(product);
        return cart;
    }

    @Override
    public Cart seeMyCart(){
        LOGGER.info("Cart Itmes are:" + cart.getCartitems().stream());
        this.cart.getCartitems().forEach(System.out::println);
        return cart;
    }


}
