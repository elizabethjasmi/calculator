package com.thoughtWorks.shoppingExperience;
import java.util.ArrayList;
import java.util.List;
public class Cart {
    List<SellableProduct> cartitems;

    public Cart() {
        this.cartitems = new ArrayList<SellableProduct>();
    }

    public Cart(List<SellableProduct> cartitems) {
        this.cartitems = cartitems;
    }

    public List<SellableProduct> getCartitems() {
        return cartitems;
    }

    public void addItemToCart(SellableProduct e){
        this.cartitems.add(e);
    }

    @Override
    public String toString() {
        return cartitems.toString();
    }
}
