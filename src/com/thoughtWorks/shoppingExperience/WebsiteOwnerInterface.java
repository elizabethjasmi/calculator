package com.thoughtWorks.shoppingExperience;

import java.util.List;

public interface WebsiteOwnerInterface extends WebsiteInterface {
    void displayAllItemsForSale();
    void displayItemsOfABrand(String brandName);
    void addItemsToSell(SellableProduct product);

}
