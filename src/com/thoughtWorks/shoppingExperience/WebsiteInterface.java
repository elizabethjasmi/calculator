package com.thoughtWorks.shoppingExperience;

import java.util.List;

public interface WebsiteInterface {
    List<SellableProduct> getAllProducts();

    List<SellableProduct> getAllProductsOfBrand(String brandName);
}
