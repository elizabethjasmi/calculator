package com.thoughtWorks.Streams;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FilterReduce {
    public static void main(String args[]){
        List<String> strings = Arrays.asList("ABC","","BCD","DCFG","JK");

        System.out.println(strings.stream().filter(s -> (s.length()>2)).collect(Collectors.toList()));


        List<String> countries = Arrays.asList("ABC", "Japan", "France", "Germany", "India", "UK","Canada");
        //System.out.println(countries.stream().map(s -> s.toUpperCase()).collect(Collectors.toList()));
        System.out.println(countries.stream().map(s -> s.toUpperCase()).collect(Collectors.joining(",")));

        List<Integer> numbers = Arrays.asList(9, 10, 3, 4, 7, 3, 4);
        System.out.println(numbers.stream().map(s -> s*s).distinct().collect(Collectors.toList()));

        List<Integer> fourthProblem = Arrays.asList(1, 2, 2, 4, 2, 5);
        System.out.println(fourthProblem.stream().filter(s -> s==2).count());


    }
}
