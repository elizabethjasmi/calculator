import java.util.Scanner;

public class Factorial {

    public int findFactorial(int number){
        int factorial = 1;
        for (int i = number; i > 1; i--) {
            factorial = factorial * i   ;
        }
        return factorial;
    }
//    public static void main(String[] args) {
//        Scanner reader = new Scanner(System.in);
//        int n = reader.nextInt();
//        int factorial = 1;
//        for (int i=n; i >1; i--) {
//            factorial = factorial * i   ;
//
//        }
//        System.out.println(factorial);
//
//    }
}
