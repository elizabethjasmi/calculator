import java.util.Scanner;

public class Palindrome {

    public static void main(String[] args) {
        String number;
        Scanner reader = new Scanner(System.in);
        number = reader.next();
        int length = number.length() - 1;
        for (int i = 0; i < length / 2; i = i + 1) {
            if (number.charAt(i) != number.charAt(length - i)) {
                System.out.println("Not Palindrome");
                return;
            }
        }
        System.out.println("It is a Palindrome");
    }
}
