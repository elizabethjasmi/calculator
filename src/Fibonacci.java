import javax.swing.*;
import java.util.Scanner;

public class Fibonacci {
    //input 10
    //output 0 1 1 2 3 5 8 13 21


    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        int n = reader.nextInt();
        int previousNumber = 0;
        int currentNumber = 1;
        int value;
        for (int i = 1; i <= n; i++) {

            System.out.println(previousNumber);
            value = previousNumber + currentNumber;
            previousNumber = currentNumber;
            currentNumber = value;
        }

    }

}
